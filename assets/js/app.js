function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
(function ($) {
  $(document).ready(function () {
     $(".toggle-btn").click(function(){
       $("header").toggleClass("toggle-header");
       $(".pop-menu").toggleClass("toggle-pop");
       $("body").toggleClass("overflow");
     });
     $(".scroll-link").on('click', function (event) {
      event.preventDefault();
        var hash = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {
            window.location.hash = hash;
        });

    });

    $('.ban1').owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      autoplay:true,
      autoplayTimeout:5000,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      },
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
    $('.owl-thought').owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      dots:false,
      autoplay:true,
      autoplayTimeout:7000,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 2
        },
        1200: {
          items: 3
        }
      },
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
    $('.owl-thought-home').owlCarousel({
      loop: false,
      margin: 10,
      nav: false,
      stagePadding:20,
      dots:false,
      autoplay:false,
      autoplayTimeout:7000,
      responsive: {
        0: {
          items: 1
        },
        767: {
          items: 1
        },
        1000: {
          items: 2
        },
        1200: {
          items: 3
        }
      },
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
    $('.owl-services').owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      autoplay:true,
      dots:true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        },
        1200: {
          items: 1
        }
      }
    });
    $('.owl-clients').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      autoplay:false,
      dots:false,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1200: {
          items: 5
        }
      },
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
    $('#demoslide').carousel({
      pause: true,
      interval: false
  });
  
  });
})(jQuery);
jQuery(function($) {
 
      var $el = $('.our-services');
      var a = $('.our-solutions').offset().top;
   
var maxScroll = a;
     
          currentScroll = 0;
      $(window).on('scroll', function () {
          var scroll = $(document).scrollTop();
          currentScroll = (-.6*scroll);
          if(Math.abs(currentScroll) >= maxScroll) { return; }
          $el.css({
              'background-position-y': (-.17*scroll)+'px'
          });
      });
    
      
 
});
$(window).scroll(function() {

  var topOfWindow = $(window).scrollTop(),
      bottomOfWindow = topOfWindow + $(window).height();

  $('.anim').each(function(){
      var imagePos = $(this).offset().top;

      if (imagePos < topOfWindow+800) {
              $(this).addClass('animate');
      }


  });

 });
